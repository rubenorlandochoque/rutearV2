package com.sistematias.rutear.clases;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.sistematias.rutear.activity.MyApp;
import com.sistematias.rutear.model.Persistencia;

import java.text.SimpleDateFormat;

/**
 * Created by usuario on 26/05/2015.
 */
public class Data {
    private static boolean syncAll = false;
    private static boolean serviceActive = false;
    public static final String DIRECTORIO_FOTO = "ruteo/fotos";
    private static String codRepositor = "";
    private static String codPuntoVentaActual = "";
    private static String nombrePuntoVentaActual = "";
    public static Repository conn;
    private static String fechaUltimoInicioSesion = "";
    private static String codSucursalActual = "";

    public static String getImei() {
        if(imei==null || imei.equals("")){
            TelephonyManager mngr = (TelephonyManager)MyApp.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
            imei = mngr.getDeviceId();
        }
        return imei;
    }

    private static String imei = "";

    public static final SimpleDateFormat FECHA_DATABASE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat FECHA_SQL_SEVER_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    public static final SimpleDateFormat FECHA_FULL_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    public static final SimpleDateFormat FECHA_SHORT_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat FECHA_SHORT_FORMAT_SLASH = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat FECHA_LAST_SESSION = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public static final SimpleDateFormat FECHA_FILE_NAME = new SimpleDateFormat("yyyyMMdd_HHmmss");


    public static boolean isSyncAll() {
        return syncAll;
    }

    public static void setSyncAll(boolean syncAll) {
        Data.syncAll = syncAll;
    }

    public static boolean isServiceActive() {
        return serviceActive;
    }

    public static void setServiceActive(boolean serviceActive) {
        Data.serviceActive = serviceActive;
    }

    public static String getCodPuntoVentaActual() {
        if(codPuntoVentaActual==null || codPuntoVentaActual.equals("")){

            Persistencia persistencia = new Persistencia();
            persistencia.getPersistenciaByCod(Persistencia.COD_PUNTO_VENTA_ACTUAL);
            codPuntoVentaActual = persistencia.getValuePersistencia();
            Log.i("entro","cod punto de venta actual: "+persistencia.getValuePersistencia());
        }
        return codPuntoVentaActual;
    }

    public static void setCodPuntoVentaActual(String codPuntoVentaActual) {
        Data.codPuntoVentaActual = codPuntoVentaActual;
    }

    public static String getCodSucursalActual() {
        if(codSucursalActual==null || codSucursalActual.equals("")){

            Persistencia persistencia = new Persistencia();
            persistencia.getPersistenciaByCod(Persistencia.COD_SUCURSAL);
            codSucursalActual = persistencia.getValuePersistencia();
            Log.i("entro","cod sucursal: "+persistencia.getValuePersistencia());
        }
        return codSucursalActual;
    }

    public static void setCodSucursalActual(String codSucursalActual) {
        Data.codSucursalActual = codSucursalActual;
    }

    public static String getCodRepositor() {
        if(codRepositor==null || codRepositor.equals("")){
            Persistencia persistencia = new Persistencia();
            persistencia.getPersistenciaByCod(Persistencia.COD_REPOSITOR);
            codRepositor = persistencia.getValuePersistencia();
            Log.i("entro","cod repositor: "+persistencia.getValuePersistencia());
        }
        return codRepositor;
    }

    public static void setCodRepositor(String codRepositor) {
        Data.codRepositor = codRepositor;
    }

    public static String getNombrePuntoVentaActual() {
        if(nombrePuntoVentaActual==null || nombrePuntoVentaActual.equals("")){

            Persistencia persistencia = new Persistencia();
            persistencia.getPersistenciaByCod(Persistencia.COD_NOMBRE_PUNTO_VENTA_ACTUAL);
            nombrePuntoVentaActual = persistencia.getValuePersistencia();
            Log.i("entro","nombre punto de venta: "+persistencia.getValuePersistencia());
        }
        return nombrePuntoVentaActual;
    }

    public static void setNombrePuntoVentaActual(String nombrePuntoVentaActual) {
        Data.nombrePuntoVentaActual = nombrePuntoVentaActual;
    }

    public static Repository getConn() {
        if(conn==null){
            conn = new Repository(MyApp.getAppContext());
        }
        return conn;
    }

    public static void setConn(Repository conn) {
        Data.conn = conn;
    }

    public static String getFechaUltimoInicioSesion() {
        if(fechaUltimoInicioSesion==null || fechaUltimoInicioSesion.equals("")){
            Persistencia persistencia = new Persistencia();
            persistencia.getPersistenciaByCod(Persistencia.COD_FECHA_ULTIMO_INICIO_SESION);
            fechaUltimoInicioSesion = persistencia.getValuePersistencia();
            Log.i("entro","nfecha ultimo iniciio de session: "+persistencia.getValuePersistencia());
        }
        return fechaUltimoInicioSesion;
    }

    public static void setFechaUltimoInicioSesion(String fechaUltimoInicioSesion) {
        Data.fechaUltimoInicioSesion = fechaUltimoInicioSesion;
    }
}
