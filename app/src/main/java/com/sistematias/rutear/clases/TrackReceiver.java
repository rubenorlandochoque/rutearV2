package com.sistematias.rutear.clases;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.sistematias.rutear.Tasks.TrackingTask;

/**
 * Created by samuel on 24/10/2015.
 */
public class TrackReceiver extends BroadcastReceiver {
    private static final String TAG = "GpsTrackerAlarmReceiver";
    @Override
    public void onReceive(Context arg0, Intent arg1) {
        new TrackingTask(arg0).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}