package com.sistematias.rutear.clases;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.sistematias.rutear.model.Tracking;

import java.util.Date;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

/**
 * Created by RUBEN on 18/07/2016.
 */
public class TrackerClass {
    static LocationTracker tracker;
    static Date fechaAnterior;
    public static void initTracking(Context context,long tiempominutos){
        final long milisegundos = tiempominutos * 60 * 1000;
        TrackerSettings settings =
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(true)
                        .setTimeBetweenUpdates(milisegundos)
                        .setMetersBetweenUpdates(1);
         tracker = new LocationTracker(context, settings) {
            @Override
            public void onLocationFound(Location location) {
                if ((new Date()).getTime() - fechaAnterior.getTime()>milisegundos){
                    fechaAnterior = new Date();
                    Log.w("correcto", "encontrado: " + location.getLatitude() + "," + location.getLongitude() + " - " + location.getProvider() + " - " + location.getAccuracy()+"  tiempo:"+((new Date()).getTime() - fechaAnterior.getTime()));
                    if(!Data.getCodPuntoVentaActual().isEmpty() && !Data.getCodSucursalActual().isEmpty()){
                        Tracking tracking = new Tracking();
                        tracking.setLatitud(location.getLatitude() + "");
                        tracking.setLongitud(location.getLongitude() + "");
                        tracking.setPrecision((int) location.getAccuracy());
                        tracking.setFechaCaptura(Data.FECHA_DATABASE_FORMAT.format(new Date()));
                        tracking.setCodRepositor(Data.getCodRepositor());
                        tracking.setCodPuntoVenta(Data.getCodPuntoVentaActual());
                        tracking.setCodSucursal(Data.getCodSucursalActual());
                        tracking.saveData();
                    }
                }
                // Do some stuff when a new location has been found.
                //if (location.getProvider().toUpperCase().contains("gps")) {
                //Log.e("nueva posicion", "encontrado: " + location.getLatitude() + "," + location.getLongitude() + " - " + location.getProvider() + " - " + location.getAccuracy()+"  tiempo:"+((new Date()).getTime() - fechaAnterior.getTime()));
                //}
            }

            @Override
            public void onTimeout() {

            }
        };
        fechaAnterior = new Date();
        tracker.startListening();
    }

    public static void stopTracking(){
        if(tracker!=null){
            try{
                tracker.stopListening();
                tracker = null;
            }catch (Exception e){}
        }
    }

}
