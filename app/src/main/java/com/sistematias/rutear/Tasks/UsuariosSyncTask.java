package com.sistematias.rutear.Tasks;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.clases.Querys;
import com.sistematias.rutear.model.Config;
import com.sistematias.rutear.model.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by RUBEN on 31/10/2015.
 */
public class UsuariosSyncTask extends AsyncTask<String, Integer, Boolean> {
    ProgressDialog loading = null;
    private Context activity;
    public static boolean exito=true;
    public static String message="";
    public UsuariosSyncTask(Context _activity){
        activity = _activity;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        exito=true;
        Config url = new Config();
        url.getConfigByCod("CONFIG_SERVER");

        Config nameSpace = new Config();
        nameSpace.getConfigByCod("CONFIG_NAMESAPCE");

        final String NAMESPACE = nameSpace.getValueConfig();
        final String URL = url.getValueConfig();
        final String METHOD_NAME = "SincronizarUsuarios";
        final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
        try{
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            try {
                HttpTransportSE transporte = new HttpTransportSE(URL);
                transporte.call(SOAP_ACTION, envelope);
                SoapObject resultado_xml = (SoapObject) envelope.bodyIn;
                String res = "";
                try {
                    res = resultado_xml.getProperty(0).toString();
                    Log.i("Web Service Result", res);
                    JSONObject data = new JSONObject(res);
                    JSONArray datosusuarios = data.getJSONArray("datosusuario");
                    ArrayList<Usuario> usuarios = new ArrayList<>();
                    for (int iz=0;iz< datosusuarios.length(); iz++)
                    {
                        JSONObject json = datosusuarios.getJSONObject(iz);
                        Usuario usuario = new Usuario();
                        usuario.setCodUsuario(json.getString("CodUsuario"));
                        usuario.setUsuario(json.getString("Usuario"));
                        usuario.setPass(json.getString("pass"));
                        usuario.setCodPuntoVenta(json.getString("CodPuntoVenta"));
                        usuario.setPuntoVenta(json.getString("PuntoVenta"));
                        usuario.setCodTipoUsuario(json.getString("CodTipoUsuario"));
                        usuario.setSincronizarTracking(json.getInt("SincronizarTracking"));

                        //Log.e("SincronizarTracking: ","SincronizarTracking: "+usuario.getSincronizarTracking());


                        try{
                            usuario.setCodSucursal(json.getString("CodSucursal"));
                        }catch (Exception e){
                            usuario.setCodSucursal("");
                        }
                        usuarios.add(usuario);
                    }
                    try {
                        if(usuarios.size()>0){
                            Data.getConn().executeSQLQuery(Querys.deleteUsuarios());
                            for(Usuario usuario:usuarios){
                                //Log.i("usuario cod",usuario.getCodUsuario()+"_____"+usuario.getUsuario());
                                usuario.saveData();
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        exito = false;
                        message=e.getMessage();
                    }

                    JSONArray datosconfiguracion = data.getJSONArray("datosconfiguracion");
                    for (int iz=0;iz< datosconfiguracion.length(); iz++)
                    {
                        JSONObject json = datosconfiguracion.getJSONObject(iz);
                        String codConfiguracion = json.getString("CodConfiguracion");
                        String valueConfiguracion = json.getString("ValueConfiguracion");
                        int id = Data.conn.getIntifExists("select idconfig from config where codconfig='"+codConfiguracion+"'");
                        if(id==-1){//no existe
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("CodConfig",codConfiguracion);
                            contentValues.put("ValueConfig", valueConfiguracion);
                            Data.getConn().insert("Config",contentValues);
                        }else{
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("ValueConfig",valueConfiguracion);
                            Data.getConn().update("Config",contentValues,"idconfig="+id,null);
                        }
                    }
                    /*Cursor c = null;
                    c = Data.conn.executeSelect("select * from config",c);
                    while (c.moveToNext()){
                        Log.e("data","valueconfig: "+c.getString(1)+" ---  codconfig: "+c.getString(2));
                    }*/
                } catch (Exception e) {
                    Log.i("ERROR", e.getMessage());
                    e.printStackTrace();
                    exito = false;
                    message=res.substring(5);
                }
            } catch (Exception e) {
                Log.i("ERROR", e.getMessage());
                e.printStackTrace();
                exito = false;
                message=e.getMessage();
            }

        }catch(Exception e){
            exito = false;
            message=e.getMessage();
            e.printStackTrace();
        }
        return true;
    }
    @Override
    protected void onPreExecute() {
        loading = new ProgressDialog(activity);
        loading.setMessage("Validando usuario");
        //loading.setCancelable(false);
//        loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                UsuariosSyncTask.this.cancel(true);
//            }
//        });
        /*loading.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                UsuariosSyncTask.this.cancel(true);
                Intent intent  =new Intent("USER_SYNC_CANCEL");
                activity.getApplicationContext().sendBroadcast(intent);
                dialog.dismiss();
            }
        });*/
        loading.show();

    }
    @Override
    protected void onPostExecute(Boolean result) {
        try{
            loading.hide();
            loading.dismiss();
        }catch(Exception e){
            e.printStackTrace();
        }
        Intent intent;
        if(exito){
            intent  =new Intent("USER_SYNC_FINISH");
        }else {
            intent  =new Intent("USER_SYNC_FAILURE");
            intent.putExtra("ErrorMessage",message);
        }
        activity.sendBroadcast(intent);

    }

}
