package com.sistematias.rutear.Tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.sistematias.rutear.Services.GPSTracker;

/**
 * Created by samuel on 24/10/2015.
 */
public class TrackingTask extends AsyncTask<String, Integer, Boolean> {
    public static final String tag = "Tracking Task-->";
    ProgressDialog loading = null;
    Boolean isCancelled = false;
    private Context activity;
    public TrackingTask(Context _activity){
        activity = _activity;
    }

    @Override
    protected Boolean doInBackground(String... params) {

        Looper looper = Looper.getMainLooper();
        Handler handler = new Handler(looper);
        handler.post(new Runnable() {
            public void run() {
                try {
                    new GPSTracker(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return true;
    }
    @Override
    protected void onPreExecute() {

    }
    @Override
    protected void onPostExecute(Boolean result) {

    }
}
