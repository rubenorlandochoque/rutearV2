package com.sistematias.rutear.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sistematias.rutear.Adapters.ArrayAdapterTipoNovedad;
import com.sistematias.rutear.R;
import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.clases.DecimalDigitsInputFilter;
import com.sistematias.rutear.clases.Util;
import com.sistematias.rutear.model.FotoNovedad;
import com.sistematias.rutear.model.Novedad;
import com.sistematias.rutear.model.Producto;
import com.sistematias.rutear.model.TipoNovedad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NovedadActivity extends AppCompatActivity {
    public final int CAMERA_REQUEST = 6000;
    Spinner spinnerTipoNovedad;
    LinearLayout llBotoneraNovedades;
    ScrollView svNovedades;
    EditText txtCodigoProducto;
    LinearLayout myGallery;
    private Uri currentImageUri;
    public static final String tag = "FotosNovedades";
    public static String codigo;
    public static int lastImageId=-1;

    //private int UPDATE_INTERVAL = 10 * 1000; // Intervalo de actualizacion. Por defecto: 10 segundos.
    //private int FASTEST_INTERVAL = 1 * 1000; // Intervalo de actualizacion rapido. Por defecto: 5 segundos.
    //private int DISPLACEMENT = 200; // Distancia en metros. Por defecto: 10 metros.
    //private int TIMEOUT = 60 * 60 * 1000; // Tiempo máximo de búsqueda. Por defecto: 5 minutos.
    /*public String latLocation = "0",
            longLocation = "0",
            distanceLocation = "0";*/

    //private static final String GPS_MANAGER_BROADCAST_RECEIVER = "GPS_MANAGER_BROADCAST_RECEIVER";
    //private static final String GPS_MANAGER_TIMEOUT_BROADCAST_RECEIVER = "GPS_MANAGER_TIMEOUT_BROADCAST_RECEIVER";

    /*private final BroadcastReceiver mGPSManagerBroadcastReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    latLocation = intent.getStringExtra("latLocation");
                    if(latLocation == null) latLocation = "0";
                    longLocation = intent.getStringExtra("longLocation");
                    if(longLocation == null) longLocation = "0";
                    distanceLocation = intent.getStringExtra("distanceLocation");
                    if(distanceLocation == null) distanceLocation = "0";

                    Log.i("tag", "NUEVA POSICIÓN ENCONTRADA >>> X: " + latLocation + ", Y: " + longLocation + ", DISTANCIA: " + distanceLocation);

                    Double distance = Double.valueOf(distanceLocation);

                    //((TextView) findViewById(R.id.searchingGPS)).setText("GPS encontrado: lat: " + latLocation + ", long: " + longLocation + ", precision: " + distanceLocation + " mts.");
                    ((TextView) findViewById(R.id.searchingGPS)).setText("GPS encontrado");
                    ((TextView) findViewById(R.id.searchingGPS)).setTextColor(getResources().getColor(R.color.result_points));

                }

            };*/
    /*private final BroadcastReceiver mGPSManagerTimeoutBroadcastReceiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    ((TextView) findViewById(R.id.searchingGPS)).setText("No se econtró ninguna posici+on gps");
                }
            };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novedad);
        getControls();
        initData();
        //((TimePicker)findViewById(R.id.timePickerHoraElaboracion)).setIs24HourView(true);
        getSupportActionBar().hide();


        ((EditText) findViewById(R.id.txtFechaElboracionNovedad)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //DialogFragment newFragment = new DatePickerFragment();
//        newFragment.show(getSupportFragmentManager(), "datePicker");
                    final Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog;
                    datePickerDialog = new DatePickerDialog(NovedadActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int day) {
                            ((EditText) findViewById(R.id.txtFechaElboracionNovedad)).setText((String.format("%02d/%02d/%04d", day, month+1, year)));
                        }

                    }, year, month, day);
                    datePickerDialog.setTitle("Seleccione fecha");
                    datePickerDialog.show();
                }
            }
        });


        ((EditText) findViewById(R.id.txtHoraElboracionNovedad)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //DialogFragment newFragment = new TimePickerFragment();
                    //newFragment.show(getSupportFragmentManager(), "timePicker");
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);

                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(NovedadActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            ((EditText) findViewById(R.id.txtHoraElboracionNovedad)).setText((String.format("%02d:%02d", selectedHour, selectedMinute)));
                        }
                    }, hour, minute, true);
                    mTimePicker.setTitle("Selecciones hora");
                    mTimePicker.show();
                }
            }
        });

        ((EditText)findViewById(R.id.txtPrecioNovedad)).setFilters(new InputFilter[]{new DecimalDigitsInputFilter(10, 2)});

        ((ScrollView)findViewById(R.id.svNovedades)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {

                }
                return false;
            }
        });

        /*if (!isGPSEnabled(MyApp.getAppContext())) {
            ((TextView) findViewById(R.id.searchingGPS)).setText("");
        } else {
            GPSService.getInstance().setParametros(UPDATE_INTERVAL, FASTEST_INTERVAL, DISPLACEMENT, TIMEOUT);
            GPSService.getInstance().iniciarBusquedaGPS2(MyApp.getAppContext());
        }*/
    }

    /*public boolean isGPSEnabled(Context mContext) {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }*/

    public void getControls() {
        spinnerTipoNovedad = (Spinner) findViewById(R.id.spinnerTipoNovedad);
        llBotoneraNovedades = (LinearLayout) findViewById(R.id.llBotoneraNovedades);
        svNovedades = (ScrollView) findViewById(R.id.svNovedades);
        txtCodigoProducto = (EditText) findViewById(R.id.txtCodigoProducto);
        myGallery = (LinearLayout) findViewById(R.id.mygallery);
    }



    public void initData() {
        ArrayList<TipoNovedad> arrayList = new ArrayList<>();
        TipoNovedad tn = new TipoNovedad();
        tn.setIdTipoNovedad(-1);
        tn.setCodTipoNovedad("COD_SELECIONES_UNA_OPCION");
        tn.setDescripcion("Seleccione una opción");
        arrayList.add(tn);
        ArrayList<TipoNovedad> novedades = TipoNovedad.getAll();
        if (novedades.size() > 0) {
            arrayList.addAll(novedades);
        }
        ArrayAdapterTipoNovedad arrayAdapterTipoNovedad = new ArrayAdapterTipoNovedad(this, arrayList);
        arrayAdapterTipoNovedad.notifyDataSetChanged();
        spinnerTipoNovedad.setAdapter(arrayAdapterTipoNovedad);
    }

    public void btnBuscarEanOnClickListener(View v){
        getProductoBySearch(txtCodigoProducto.getText().toString().trim());
    }

    public void btnScanearProductoNovedadOnClickListener(View v){
        scanBarcodeCustomLayout();
    }

    public void btnGuardarNovedadOnClickListener(View v) {
        String validar = validar();
        if(!validar.equals("EXITO")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Aviso");
            builder.setMessage(validar);
            builder.setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
            return;
        }

        guardarNovedad();
        /*if(((TextView) findViewById(R.id.searchingGPS)).getText().toString().trim().contains("Buscando")){
            AlertDialog.Builder builder = new AlertDialog.Builder(NovedadActivity.this);
            builder.setTitle("Aviso");
            builder.setMessage("GPS no encontrado.")
                    .setCancelable(false)
                    .setPositiveButton("Esperar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    })
                    .setNeutralButton("Guardar\nsin GPS",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            guardarNovedad();
                        }
                    })
                    .setNegativeButton("Salir\nsin guardar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }else {
            guardarNovedad();
        }*/
    }

    public void guardarNovedad(){
        Producto producto = new Producto();
        producto.getProductoByEAN(txtCodigoProducto.getText().toString().trim());

        Novedad novedad = new Novedad();
        novedad.setCodNovedad("");
        novedad.setCodProducto(producto.getCodProducto());
        novedad.setCodRepositor(Data.getCodRepositor());
        novedad.setCodPuntoVenta(Data.getCodPuntoVentaActual());
        String codTipoNovedad = ((TipoNovedad) spinnerTipoNovedad.getSelectedItem()).getCodTipoNovedad();
        if (!codTipoNovedad.equals("COD_SELECIONES_UNA_OPCION")) {
            novedad.setCodTipoNovedad(codTipoNovedad);
        }
        novedad.setObservacion(((EditText) findViewById(R.id.txtObservacionNovedad)).getText().toString().trim());
        novedad.setNumLote(((EditText) findViewById(R.id.txtNumLoteNovedad)).getText().toString().trim());
        try {
            novedad.setCantidad(Integer.parseInt(((EditText) findViewById(R.id.txtCantidadNovedad)).getText().toString().trim()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            novedad.setPrecio(Double.parseDouble(((EditText) findViewById(R.id.txtPrecioNovedad)).getText().toString().trim()));
        } catch (Exception pe) {

        }
        novedad.setFechaCreacion(Data.FECHA_DATABASE_FORMAT.format(new Date()));
        String fechaElaboracion = ((EditText) findViewById(R.id.txtFechaElboracionNovedad)).getText().toString().trim();
        if (!fechaElaboracion.equals("")) {
            try {
                Date d = Data.FECHA_SHORT_FORMAT_SLASH.parse(fechaElaboracion);
                novedad.setFechaElaboracion(Data.FECHA_DATABASE_FORMAT.format(d));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        novedad.setHoraElaboracion(((EditText) findViewById(R.id.txtHoraElboracionNovedad)).getText().toString().trim());
        novedad.setCodSucursal(Data.getCodSucursalActual());
        //Log.w("hola novedad",latLocation + ";" + longLocation);
        novedad.setCoordenadas("0;0");
        //double d = Double.parseDouble(distanceLocation);
        novedad.setRango(0);


//        if (mBound) {
//            //Location num = gpsTracker.getLocation(this);
////            Toast.makeText(this, "longitud: " + gpsTracker.getLatitude() , Toast.LENGTH_SHORT).show();
//            if (gpsTracker.canGetLocation()) {
//                double latitude = gpsTracker.getLatitude();
//                double longitude = gpsTracker.getLongitude();
//                double rango = gpsTracker.getAccuracy();
//                novedad.setCoordenadas(latitude + ";" + longitude);
//                novedad.setRango((int) rango);
//            } else {
//
//            }
//
//        }

//        if (gps.canGetLocation()) {
//            Location location = gps.getLocation(this);
//            double latitude = location.getLatitude();
//            double longitude = location.getLongitude();
//            double rango = location.getAccuracy();
//            novedad.setCoordenadas(latitude + ";" + longitude);
//            novedad.setRango((int) rango);
//        } else {
//
//        }

        String codNovedad = novedad.saveData();

        /*Guardar las fotos*/
        LinearLayout gallery = (LinearLayout) findViewById(R.id.mygallery);
        for (int i = 0; i < gallery.getChildCount(); i++) {
            if (gallery.getChildAt(i).getClass().equals(LinearLayout.class)) {
                LinearLayout photoLayout = (LinearLayout) gallery.getChildAt(i);
                for (int j = 0; j < photoLayout.getChildCount(); j++) {
                    if (photoLayout.getChildAt(j).getClass().equals(ImageView.class)) {
                        ImageView image = (ImageView) photoLayout.getChildAt(j);
                        try {
                            FotoNovedad fotoNovedad = new FotoNovedad();
                            fotoNovedad.setCodFotoNovedad("");
                            fotoNovedad.setCodNovedad(codNovedad);
                            String path = image.getTag().toString();
                            Log.i("path gallery", path);
                            if (!path.trim().equals("")) {
                                String[] patharray = path.split(File.separator);
                                String name = "";
                                if (patharray.length > 0) {
                                    name = patharray[patharray.length - 1];
                                }
                                fotoNovedad.setImagen(name);
                            }
                            fotoNovedad.saveData();
                        } catch (Exception w) {

                        }


                    }
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(NovedadActivity.this);
        builder.setMessage("Guardado correcto")
                .setCancelable(false)
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void takePhotoOnClickListener(View v){
        currentImageUri = Util.getImageFileUri(codigo, NovedadActivity.this);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri);
        lastImageId = Util.getLastImageId();
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    public void btnCancelarNovedadOnClickListener(View v) {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_novedad, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        250, 250);
                layoutParams.setMargins(0, 35, 0, 0);

                myGallery.addView(insertPhoto(currentImageUri.getPath()),layoutParams);
//                    fotoRuteo.setImageBitmap(null);
//                    Bitmap bm = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(currentImageUri.getPath()), 384, 384);
//                    fotoRuteo.setImageBitmap(bm);
//                    fotoRuteo.setTag(currentImageUri.getPath().toString());
//                    fotoRuteo.setVisibility(View.VISIBLE);
                int actualLastImageId = Util.getLastImageId();
                if (actualLastImageId > lastImageId) {
                    Util.removeImage(actualLastImageId);
                }
            }
        }


        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("ean");
                getProducto(contents);
            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelado", Toast.LENGTH_LONG).show();
                //llBotoneraNovedades.setVisibility(View.GONE);
                //svNovedades.setVisibility(View.GONE);
            }
        }
    }

    public void  getProducto(String ean){
        codigo = ean;
        Producto producto = new Producto();
        producto.getProductoByEAN(ean);
        if (producto.getEan().trim().equals("")) {
            //llBotoneraNovedades.setVisibility(View.GONE);
            //svNovedades.setVisibility(View.GONE);
            ((EditText)findViewById(R.id.txtNombreProducto)).setText("");
            showNotFound(ean);
        } else {
            //llBotoneraNovedades.setVisibility(View.VISIBLE);
            //svNovedades.setVisibility(View.VISIBLE);
            txtCodigoProducto.setText(ean);
            ((EditText)findViewById(R.id.txtNombreProducto)).setText(producto.getNombre());

        }
    }

    public void  getProductoBySearch(String ean){
        codigo = ean;
        Producto producto = new Producto();
        producto.getProductoByEAN(ean);
        if (producto.getEan().trim().equals("")) {
            //llBotoneraNovedades.setVisibility(View.GONE);
            //svNovedades.setVisibility(View.GONE);
            ((EditText)findViewById(R.id.txtNombreProducto)).setText("");
            showNotFoundSearch(ean);

        } else {
            try {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                //((LinearLayout)findViewById(R.id.layout_botonera)).setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }
            //llBotoneraNovedades.setVisibility(View.VISIBLE);
            //svNovedades.setVisibility(View.VISIBLE);
            txtCodigoProducto.setText(ean);
            ((EditText)findViewById(R.id.txtNombreProducto)).setText(producto.getNombre());

        }
    }

    public void showNotFound(String ean) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NovedadActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("No se encontró ningún registro para el código:\n" + ean);
        alertDialog.setPositiveButton("Reintentar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        scanBarcodeCustomLayout();
                    }
                });
        alertDialog.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void showNotFoundSearch(String ean) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NovedadActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("No se encontró ningún registro para el código:\n"+ean);
        alertDialog.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void scanBarcodeCustomLayout() {
        Intent in = new Intent(NovedadActivity.this,ScanActivity.class);
        startActivityForResult(in,0);
    }

    public View insertPhoto(String path) {
        Log.i("insetphotopath", path);
//        Bitmap bm = Util.decodeSampledBitmapFromUri(path, 220, 220);

        final LinearLayout layout = new LinearLayout(getApplicationContext());
        layout.setLayoutParams(new Toolbar.LayoutParams(250, 250));
        layout.setGravity(Gravity.CENTER);


        try {
            InputStream ims = getContentResolver().openInputStream(currentImageUri);
            // just display image in imageview
            Bitmap imageBitmap = BitmapFactory.decodeStream(ims);
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 384, 384, false);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            ImageView imageView = new ImageView(getApplicationContext());
            imageView.setLayoutParams(new Toolbar.LayoutParams(220, 220));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(imageBitmap);
            imageView.setTag(path);
            layout.addView(imageView);
            imageView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(NovedadActivity.this);
                    builder.setMessage("¿Borrar foto?")
                            .setCancelable(true)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    myGallery.removeView(layout);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    return false;
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



        return layout;
    }


    public void showOnBackPressedMessage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NovedadActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("¿Salir sin guardar?");
        alertDialog.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void showTimePickerDialog(View v) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(NovedadActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                ((EditText) findViewById(R.id.txtHoraElboracionNovedad)).setText((String.format("%02d:%02d", selectedHour, selectedMinute)));
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Seleccione hora");
        mTimePicker.show();
    }

    public void showDatePickerDialog(View v) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(NovedadActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                ((EditText) findViewById(R.id.txtFechaElboracionNovedad)).setText((String.format("%02d/%02d/%04d", day, month+1,year)));
            }

        }, year, month, day);
        datePickerDialog.setTitle("Seleccione fecha");
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        showOnBackPressedMessage();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        //Log.i("service","service start");
        //Intent intent = new Intent(this, GPSTracker.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    //GPSTracker gpsTracker;
    //boolean mBound = false;
    /** Defines callbacks for service binding, passed to bindService() */
    /*private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Log.i("local binder","at to local binder");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            GPSTracker.LocalBinder binder = (GPSTracker.LocalBinder) service;
            gpsTracker = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };*/

    @Override
    public void onResume() {
        super.onResume();
        /*IntentFilter filter = new IntentFilter(GPS_MANAGER_BROADCAST_RECEIVER);
        this.registerReceiver(mGPSManagerBroadcastReceiver, filter);

        IntentFilter filter2 = new IntentFilter(GPS_MANAGER_TIMEOUT_BROADCAST_RECEIVER);
        this.registerReceiver(mGPSManagerTimeoutBroadcastReceiver, filter2);*/
    }

    @Override
    public void onPause() {
        super.onPause();
        //this.unregisterReceiver(mGPSManagerBroadcastReceiver);
    }

    public String validar(){

        if(((TipoNovedad)((Spinner) findViewById(R.id.spinnerTipoNovedad)).getSelectedItem()).getCodTipoNovedad().equals("COD_SELECIONES_UNA_OPCION")){
            return "Debe seleccionar un tipo de novedad";
        }

        if(((EditText) findViewById(R.id.txtObservacionNovedad)).getText().toString().trim().isEmpty()){
            return "Debe completar el campo observación";
        }

        return "EXITO";
    }
}
