package com.sistematias.rutear.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sistematias.rutear.Adapters.ArrayAdapterPuntosVentas;
import com.sistematias.rutear.R;
import com.sistematias.rutear.Services.Prefs;
import com.sistematias.rutear.Services.TrackerService;
import com.sistematias.rutear.Tasks.TrackingSyncTask;
import com.sistematias.rutear.Tasks.UsuariosSyncTask;
import com.sistematias.rutear.clases.ConnectionChecker;
import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.clases.Repository;
import com.sistematias.rutear.clases.TrackerClass;
import com.sistematias.rutear.model.Config;
import com.sistematias.rutear.model.Persistencia;
import com.sistematias.rutear.model.PuntoVenta;
import com.sistematias.rutear.model.Usuario;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

public class LoginActivity extends AppCompatActivity {
    Boolean showPass = false;
    //GPSTracker gpsTracker;
    //boolean mBound = false;
    static int time = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        RxPermissions.getInstance(LoginActivity.this).request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION).subscribe(grandted -> {
            if (!grandted) {
                String error = "Se requiere todos los permisos para poder utilizar la aplicación";
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(error)
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        setEvents();
        getSupportActionBar().hide();

        Intent intent = getIntent();
        boolean disabled = false;
        try {
            String d = intent.getStringExtra("disabled");
            if (d != null && d.equals("disabled")) {
                disabled = true;

            }
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }

        if (false) {
            //if (disabled) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Configuración GPS");
            alertDialog.setMessage("Se deshabilitó GPS.\n Para poder utilizar esta aplicacción es necesario habilitar GPS.\n¿Desea habilitar GPS?");
            alertDialog.setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    LoginActivity.this.startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
        Data.getConn();

        findViewById(R.id.logo).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent ConfiguracionActivityIntent = new Intent(LoginActivity.this, ConfiguracionActivity.class);
                startActivity(ConfiguracionActivityIntent);
                return false;
            }
        });


//        LocationManager lm = (LocationManager) getSystemService(MyApp.getAppContext().LOCATION_SERVICE);
//        lm.addGpsStatusListener(new GpsStatus.Listener() {
//            @Override
//            public void onGpsStatusChanged(int event) {
//                switch (event) {
//                    case GpsStatus.GPS_EVENT_STARTED:
//                        Log.i("gps start","gps start");
//                        break;
//                    case GpsStatus.GPS_EVENT_STOPPED:
//                        Log.i("gps finish","gps finish");
//                        break;
//                }
//            }
//        });

        ((EditText) findViewById(R.id.passLogin)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validarLogueo();
                }
                return false;
            }
        });
    }

    private void setEvents() {

        ((EditText) findViewById(R.id.passLogin)).setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    public void btnLoginOnClickListener(View v) {
        RxPermissions.getInstance(LoginActivity.this).request(Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION).subscribe(grandted -> {
            if (!grandted) {
                String error = "Se requiere todos los permisos para poder utilizar la aplicación";
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(error)
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                if (isOnline()) {
                    UsuariosSyncTask asyncTask = new UsuariosSyncTask(LoginActivity.this);
                    asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    validarLogueo();
                }

            }
        });
    }

    public void validarLogueo() {


        EditText userLogin = (EditText) findViewById(R.id.userLogin);
        EditText passLogin = (EditText) findViewById(R.id.passLogin);

        try {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(passLogin.getWindowToken(), 0);
        } catch (Exception e) {

        }

        try {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(userLogin.getWindowToken(), 0);
        } catch (Exception e) {

        }


        String user = userLogin.getText().toString().trim();
        String pass = passLogin.getText().toString().trim();
        if (!user.equals("") && !pass.equals("")) {
//            if (!isGPSEnabled(LoginActivity.this)) {
//                showSettingsAlert();
//            } else {
            Persistencia.limpiarPersistencia();

//        if (mBound && !Data.isServiceActive()) {
//            Log.i("time", time + "");
//            Location num = gpsTracker.getLocation(LoginActivity.this, time);
//            Toast.makeText(LoginActivity.this, "longitud: " + num.getLatitude(), Toast.LENGTH_SHORT).show();
//            Data.setServiceActive(true);
//        }
//        Intent activityChangeIntent = new Intent(LoginActivity.this, MenuPrincipalActivity.class);
//        startActivity(activityChangeIntent);

            Usuario usuario = checkUsuario(user, pass);
            if (!usuario.getCodUsuario().trim().equals("")) {
                /*inicia la tarea de sincronizacion de tracking*/
                /*intentar detener la tarea si ya se estaba ejecutando*/
                try {
                    ConnectionChecker.getInstance().setIsWorking(false);
                    ConnectionChecker.getInstance().getAsyncTask().cancel(true);
                    ConnectionChecker.getInstance().setAsyncTask(null);
                } catch (Exception e2) {

                }

                int frecuenciaSincronizacionTracking = 5;//minutos
                try {
                    Config frecuenciaTracking = new Config();
                    frecuenciaTracking.getConfigByCod("CONFIG_FREQUENCY_TRACKING_SYNC");
                    frecuenciaSincronizacionTracking = Integer.parseInt(frecuenciaTracking.getValueConfig());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ConnectionChecker.getInstance().setRetraso(1000 * 60 * frecuenciaSincronizacionTracking);//frecuencia en milisegundos
                ConnectionChecker.getInstance().setIsWorking(false);
                ConnectionChecker.getInstance().startConnectionCheck(this, new TrackingSyncTask(LoginActivity.this));

                if (usuario.getCodTipoUsuario().equals(Usuario.COD_REPOSITOR)) {
                    //if (!isGPSEnabled(LoginActivity.this)) {
                    //showSettingsAlert();
                    //} else {
                    if (usuario.getPuntosVentas().size() > 0) {
                        try {
                            String lastSession = usuario.getFechaUltimoInicioSesion();
                            Date d;
                            if (lastSession.trim().equals("")) {
                                d = new Date();
                            } else {
                                d = Data.FECHA_DATABASE_FORMAT.parse(lastSession);
                            }

                            Data.setFechaUltimoInicioSesion(Data.FECHA_LAST_SESSION.format(d));
                            Persistencia persistencia = new Persistencia();
                            persistencia.getPersistenciaByCod(Persistencia.COD_FECHA_ULTIMO_INICIO_SESION);
                            persistencia.setValuePersistencia(Data.FECHA_DATABASE_FORMAT.format(new Date()));
                            persistencia.setData();
                            usuario.setFechaUltimoInicioSesion(Data.FECHA_DATABASE_FORMAT.format(new Date()));
                            usuario.setFechaUltimoInicioSesion();
                            persistencia = new Persistencia();
                            persistencia.getPersistenciaByCod(Persistencia.COD_TIPO_USUARIO);
                            persistencia.setValuePersistencia(usuario.getCodTipoUsuario());
                            persistencia.setData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Data.setCodRepositor(usuario.getCodUsuario());
                        Persistencia persistencia = new Persistencia();
                        persistencia.getPersistenciaByCod(Persistencia.COD_REPOSITOR);
                        persistencia.setValuePersistencia(usuario.getCodUsuario());
                        persistencia.setData();


                        showSeleccionPuntoVenta(usuario);
                    } else {
                        showError("No existen puntos de venta asocidados al repositor");
                    }
                    //}
                } else {
                    try {
                        String lastSession = usuario.getFechaUltimoInicioSesion();
                        Date d;
                        if (lastSession.trim().equals("")) {
                            d = new Date();
                        } else {
                            d = Data.FECHA_DATABASE_FORMAT.parse(lastSession);
                        }

                        Data.setFechaUltimoInicioSesion(Data.FECHA_LAST_SESSION.format(d));
                        Persistencia persistencia = new Persistencia();
                        persistencia.getPersistenciaByCod(Persistencia.COD_FECHA_ULTIMO_INICIO_SESION);
                        persistencia.setValuePersistencia(Data.FECHA_DATABASE_FORMAT.format(new Date()));
                        persistencia.setData();
                        usuario.setFechaUltimoInicioSesion(Data.FECHA_DATABASE_FORMAT.format(new Date()));
                        usuario.setFechaUltimoInicioSesion();
                        persistencia = new Persistencia();
                        persistencia.getPersistenciaByCod(Persistencia.COD_TIPO_USUARIO);
                        persistencia.setValuePersistencia(usuario.getCodTipoUsuario());
                        persistencia.setData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Data.setCodRepositor(usuario.getCodUsuario());
                    Persistencia persistencia = new Persistencia();
                    persistencia.getPersistenciaByCod(Persistencia.COD_REPOSITOR);
                    persistencia.setValuePersistencia(usuario.getCodUsuario());
                    persistencia.setData();

                    Persistencia persistenciaCodPVA = new Persistencia();
                    persistenciaCodPVA.getPersistenciaByCod(Persistencia.COD_PUNTO_VENTA_ACTUAL);
                    persistenciaCodPVA.setValuePersistencia("");
                    persistenciaCodPVA.setData();

                    Persistencia persistenciaPVA = new Persistencia();
                    persistenciaPVA.getPersistenciaByCod(Persistencia.COD_NOMBRE_PUNTO_VENTA_ACTUAL);
                    persistenciaPVA.setValuePersistencia("");
                    persistenciaPVA.setData();

                    Persistencia persistenciaSucursal = new Persistencia();
                    persistenciaSucursal.getPersistenciaByCod(Persistencia.COD_SUCURSAL);
                    persistenciaSucursal.setValuePersistencia("");
                    persistenciaSucursal.setData();




                    /*detener servicio de tracking*/
                    if (TrackerService.isRunning()) {
                        stopService(new Intent(LoginActivity.this, TrackerService.class));
                    }
                    /*if (mBound && Data.isServiceActive()) {
                        Log.i("frecuencia de tracking", time + "");
                        gpsTracker.stopUsingGPS();
                        Data.setServiceActive(false);
                    }*/

                    userLogin.setText("");
                    passLogin.setText("");
                    Intent activityChangeIntent = new Intent(LoginActivity.this, MenuPrincipalActivity.class);
                    startActivity(activityChangeIntent);
                }
            } else {
                showError("El usuario o contraseña ingresado no son válidos");
            }
//            }
        } else {
            if (user.equals("") && pass.equals("")) {
                Toast.makeText(LoginActivity.this, "Debe ingresar usuario y contraseña", Toast.LENGTH_SHORT).show();
            } else {
                if (user.equals("")) {
                    Toast.makeText(LoginActivity.this, "Debe ingresar usuario", Toast.LENGTH_SHORT).show();
                } else {
                    if (pass.equals("")) {
                        Toast.makeText(LoginActivity.this, "Debe ingresar contraseña", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public void btnSyncUsersOnClickListener(View v) {
        if (isOnline()) {
            copyDatabaseToSD();
            UsuariosSyncTask asyncTask = new UsuariosSyncTask(LoginActivity.this);
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            Toast.makeText(LoginActivity.this, "No hay internet", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void login_password_visibility_listener(View v) {
        EditText passLogin = (EditText) findViewById(R.id.passLogin);
        if (showPass) {
            passLogin.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            passLogin.setSelection(passLogin.getText().length());
            showPass = false;
        } else {
            passLogin.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passLogin.setSelection(passLogin.getText().length());
            showPass = true;
        }
    }

    public ArrayList<PuntoVenta> puntoVentaArrayList = new ArrayList<>();

    private void showSeleccionPuntoVenta(final Usuario usuario) {
        Vector<PuntoVenta> puntosVentas = usuario.getPuntoVentaDelRepositor();
        puntoVentaArrayList.clear();
        for (int i = 0; i < puntosVentas.size(); i++) {
            puntoVentaArrayList.add(puntosVentas.get(i));
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        ArrayAdapterPuntosVentas adapter = new ArrayAdapterPuntosVentas(this, puntoVentaArrayList);
        alertDialogBuilder
                .setTitle("Seleccionar un punto de venta")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PuntoVenta pv = puntoVentaArrayList.get(which);
                                Data.setCodPuntoVentaActual(pv.getCodPuntoVenta());
                                Persistencia persistenciaCodPVA = new Persistencia();
                                persistenciaCodPVA.getPersistenciaByCod(Persistencia.COD_PUNTO_VENTA_ACTUAL);
                                persistenciaCodPVA.setValuePersistencia(pv.getCodPuntoVenta());
                                persistenciaCodPVA.setData();

                                Data.setNombrePuntoVentaActual(pv.getNombre());
                                Persistencia persistenciaPVA = new Persistencia();
                                persistenciaPVA.getPersistenciaByCod(Persistencia.COD_NOMBRE_PUNTO_VENTA_ACTUAL);
                                persistenciaPVA.setValuePersistencia(pv.getNombre());
                                persistenciaPVA.setData();

                                Data.setCodSucursalActual(pv.getCodSucursal());
                                Persistencia persistenciaSucursal = new Persistencia();
                                persistenciaSucursal.getPersistenciaByCod(Persistencia.COD_SUCURSAL);
                                persistenciaSucursal.setValuePersistencia(pv.getCodSucursal());
                                persistenciaSucursal.setData();


                                try {
                                    Config frecuenciaTracking = new Config();
                                    frecuenciaTracking.getConfigByCod("CONFIG_FREQUENCY_TRACKING");
                                    time = Integer.parseInt(frecuenciaTracking.getValueConfig());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.i("tiempo frecuencia track", time + "");
                                Prefs.putUpdateFreq(LoginActivity.this, Integer.toString(time));
                                TrackerClass.stopTracking();
                                //if (TrackerService.isRunning()) {
                                //    stopService(new Intent(LoginActivity.this, TrackerService.class));
                                //}
                                if (usuario.getSincronizarTracking() == 1) {
                                    //startService(new Intent(LoginActivity.this, TrackerService.class));
                                    TrackerClass.initTracking(LoginActivity.this,time);
                                }

                            /*if (mBound && !Data.isServiceActive()) {
                                Log.i("tiempo frecuencia trac", time + "");
                                Location num = gpsTracker.getLocation(LoginActivity.this, time);
                                Data.setServiceActive(true);

                            }*/

                                EditText userLogin = (EditText) findViewById(R.id.userLogin);
                                EditText passLogin = (EditText) findViewById(R.id.passLogin);
                                userLogin.setText("");
                                passLogin.setText("");

                                Intent activityChangeIntent = new Intent(LoginActivity.this, MenuPrincipalActivity.class);

                                startActivity(activityChangeIntent);
                            }
                        }

                );
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setTitle("Aviso")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private Usuario checkUsuario(String user, String pass) {
        Usuario usuario = new Usuario();
        usuario.checkUsuario(user, pass);
        return usuario;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void copyDatabaseToSD() {
        File root = new File(Environment.getExternalStorageDirectory(), "ruteo/backup");
        if (!root.exists()) {
            root.mkdirs();
        }
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String imei = mngr.getDeviceId();

        SimpleDateFormat formatterfull = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String fecha = formatterfull.format(new Date());

        String file = "ruteo/backup/ruteo_" + imei + ".sqlite";
        try {
            String PACKAGE_NAME = getApplicationContext().getPackageName();
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            if (sd.canWrite()) {
                String currentDBPath = "/data/" + PACKAGE_NAME + "/databases/" + Repository.getDataBaseName();
                String backupDBPath = file;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }

        } catch (Exception e) {
            Log.e("error", e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        //Log.i("service", "service start");
        //Intent intent = new Intent(this, GPSTracker.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }


    /**
     * Defines callbacks for service binding, passed to bindService()
     */
/*private ServiceConnection mConnection = new ServiceConnection() {

    @Override
    public void onServiceConnected(ComponentName className,
                                   IBinder service) {
        Log.i("local binder", "at to local binder");
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        GPSTracker.LocalBinder binder = (GPSTracker.LocalBinder) service;
        //gpsTracker = binder.getService();
        //mBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        //mBound = false;
    }
};*/
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Configuración GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS no está habilitado.\n Para poder utilizar esta aplicacción es necesario habilitar GPS.\n¿Desea habilitar GPS?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                LoginActivity.this.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public boolean isGPSEnabled(Context mContext) {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter("USER_SYNC_FINISH");
        filter.addAction("USER_SYNC_FAILURE");
        filter.addAction("USER_SYNC_CANCEL");
        this.registerReceiver(broadcastReceiver, filter);

    }

    public void showSuccessfullySyncMessage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Éxito");

        // Setting Dialog Message
        alertDialog.setMessage("La sincronización de usuarios finalizó con éxito.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message

        alertDialog.show();
    }

    public void showFailureSyncMessage(String errorMesage) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Error");
        // Setting Dialog Message
        if (errorMesage.equals("")) {
            alertDialog.setMessage("Falló la sincronización de usuarios.");
        } else {
            alertDialog.setMessage("Falló la sincronización de usuarios.\nDetalle:\n" + errorMesage);
        }

        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void showCancelSyncMessage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Cancelado");
        // Setting Dialog Message
        alertDialog.setMessage("La sicnronización de usuarios fue cancelada.");

        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i("action result", action);
            switch (action) {
                case "USER_SYNC_FINISH":
                    //showSuccessfullySyncMessage();
                    validarLogueo();
                    break;
                case "USER_SYNC_FAILURE":
                    //String errorMessage = intent.getStringExtra("ErrorMessage");
                    //showFailureSyncMessage(errorMessage);
                    validarLogueo();
                    break;
                case "USER_SYNC_CANCEL":
                    //showCancelSyncMessage();
                    validarLogueo();
                    break;
            }
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        //this.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        finish();
        try {
            this.unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            this.unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
    }
}
