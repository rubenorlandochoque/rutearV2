package com.sistematias.rutear.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.sistematias.rutear.R;
import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.model.Producto;

import java.io.File;

public class NuevoRuteoActivity extends AppCompatActivity {

    public ScrollView resultScan;
    public TextView productCode;
    public TextView nombreProducto;
    public TextView marcaProducto;
    public TextView presentacionProducto;
    public Button btnContinuarRuteo;
    public Button btnAltaProducto;
    public ImageView img_cargando;
    static File mediaStorageDir;
    public Button btnScan;
    public LinearLayout llBotoneraNuevoRuteo;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_ruteo);
        getSupportActionBar().hide();
        resultScan =(ScrollView)findViewById(R.id.resultScan);
        productCode = (TextView)findViewById(R.id.productCode);
        nombreProducto = (TextView)findViewById(R.id.nombreProducto);
        marcaProducto = (TextView)findViewById(R.id.marcaProducto);
        presentacionProducto = (TextView)findViewById(R.id.presentacionProducto);
        btnContinuarRuteo = (Button)findViewById(R.id.btnContinuarRuteo);
        btnAltaProducto = (Button)findViewById(R.id.btnAltaProducto);
        btnScan = (Button)findViewById(R.id.btnScan);
        img_cargando = (ImageView)findViewById(R.id.img_cargando);
        resultScan.setVisibility(View.GONE);
        llBotoneraNuevoRuteo = (LinearLayout)findViewById(R.id.llBotoneraNuevoRuteo);
        llBotoneraNuevoRuteo.setVisibility(View.GONE);
        initEvent();
    }


    public void scanBarcodeCustomLayout() {
        Intent in = new Intent(NuevoRuteoActivity.this,ScanActivity.class);
        startActivityForResult(in,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {

                String contents = data.getStringExtra("ean");
                //String format = data.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan

                String ean = contents;
                Producto producto = new Producto();
                producto.getProductoByEAN(ean);
                if(producto.getEan().trim().equals("")){
                    //btnAltaProducto.setVisibility(View.VISIBLE);
                    btnContinuarRuteo.setVisibility(View.GONE);
                    resultScan.setVisibility(View.GONE);
                    //btnScan.setVisibility(View.GONE);
                    showNotFound(ean);
                    llBotoneraNuevoRuteo.setVisibility(View.GONE);

                }else{
                    //btnAltaProducto.setVisibility(View.GONE);
                    //btnScan.setVisibility(View.VISIBLE);
                    llBotoneraNuevoRuteo.setVisibility(View.VISIBLE);
                    btnContinuarRuteo.setVisibility(View.VISIBLE);
                    Log.d("MainActivity", "Scanned");
                    //Toast.makeText(this, "Resultado: " + result.getContents(), Toast.LENGTH_LONG).show();
                    resultScan.setVisibility(View.VISIBLE);
                    productCode.setText(ean);
                    nombreProducto.setText(producto.getNombre());
                    marcaProducto.setText(producto.getMarca());
                    presentacionProducto.setText(producto.getPresentacion());
                    ((TextView)findViewById(R.id.txtPropioProducto)).setText(producto.isPropio() ? "Si" : "No");
                    ((TextView)findViewById(R.id.txtFamilia)).setText(producto.getFamilia());

                    /*set imageproducto*/
                    mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Data.DIRECTORIO_FOTO);
                    File image = new File(mediaStorageDir, producto.getImagen());
                    img_cargando.setImageBitmap(null);
                    if(image.exists()){
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        Bitmap bitmap = BitmapFactory.decodeFile(image.getPath(), options);
                        img_cargando.setImageBitmap(bitmap);
                    }else{
                        img_cargando.setBackgroundResource(R.drawable.imagennodisponible);
                    }
                }

            } else if (resultCode == RESULT_CANCELED) {
                // Handle cancel
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelado", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                btnContinuarRuteo.setVisibility(View.GONE);
                resultScan.setVisibility(View.GONE);
                llBotoneraNuevoRuteo.setVisibility(View.GONE);
                String action = data.getAction();
                if(action!=null){
                    if(action.equals("NUEVO")){
                        scanBarcodeCustomLayout();
                    }
                    if(action.equals("SALIR")){
                        finish();
                    }
                }
            }
            if(resultCode==RESULT_CANCELED){

            }
        }
    }

    public void getProducto(String ean){
        Producto producto = new Producto();
        producto.getProductoByEAN(ean);
        if(producto.getEan().trim().equals("")){
            //btnAltaProducto.setVisibility(View.VISIBLE);
            btnContinuarRuteo.setVisibility(View.GONE);
            resultScan.setVisibility(View.GONE);
            //btnScan.setVisibility(View.GONE);
            showNotFoundSearch(ean);
            llBotoneraNuevoRuteo.setVisibility(View.GONE);

        }else{

            try {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                //((LinearLayout)findViewById(R.id.layout_botonera)).setVisibility(View.VISIBLE);
            } catch (Exception e) {

            }
            //btnAltaProducto.setVisibility(View.GONE);
            //btnScan.setVisibility(View.VISIBLE);
            llBotoneraNuevoRuteo.setVisibility(View.VISIBLE);
            btnContinuarRuteo.setVisibility(View.VISIBLE);
            Log.d("MainActivity", "Scanned");
            //Toast.makeText(this, "Resultado: " + result.getContents(), Toast.LENGTH_LONG).show();
            resultScan.setVisibility(View.VISIBLE);
            productCode.setText(ean);
            nombreProducto.setText(producto.getNombre());
            marcaProducto.setText(producto.getMarca());
            presentacionProducto.setText(producto.getPresentacion());
            ((TextView)findViewById(R.id.txtPropioProducto)).setText(producto.isPropio() ? "Si" : "No");
            ((TextView)findViewById(R.id.txtFamilia)).setText(producto.getFamilia());

                    /*set imageproducto*/
            mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Data.DIRECTORIO_FOTO);
            File image = new File(mediaStorageDir, producto.getImagen());
            img_cargando.setImageBitmap(null);
            if(image.exists()){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(image.getPath(), options);
                img_cargando.setImageBitmap(bitmap);
            }else{
                img_cargando.setBackgroundResource(R.drawable.imagennodisponible);
            }
        }
    }

    private void initEvent(){
        btnContinuarRuteo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRuteo = new Intent(NuevoRuteoActivity.this, RuteoActivity.class);
                intentRuteo.putExtra("codigo",productCode.getText());
                startActivityForResult(intentRuteo,10);
            }
        });

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanBarcodeCustomLayout();
            }
        });
    }

    public void showNotFound(final String ean) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NuevoRuteoActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("No se encontró ningún registro para el código:\n" + ean);
        alertDialog.setPositiveButton("Reintentar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        scanBarcodeCustomLayout();
                    }
                });
        alertDialog.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
//        alertDialog.setNeutralButton("Nuevo",new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(NuevoRuteoActivity.this,AltaProductos.class);
//                intent.putExtra("codigo",ean);
//                startActivity(intent);
//            }
//        });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void showNotFoundSearch(String ean) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NuevoRuteoActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("No se encontró ningún registro para el código:\n"+ean);
        alertDialog.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public void btnBuscarEanOnClickListener(View v){
        getProducto(((EditText) findViewById(R.id.txtCodigoProducto)).getText().toString().trim());
    }


    public void showOnBackPressedMessage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                NuevoRuteoActivity.this);
        alertDialog.setTitle("Aviso");
        alertDialog.setMessage("¿Salir sin guardar?");
        alertDialog.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showOnBackPressedMessage();
    }
}

