package com.sistematias.rutear.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sistematias.rutear.Adapters.ArrayAdapterPuntosVentas;
import com.sistematias.rutear.R;
import com.sistematias.rutear.clases.ConnectionChecker;
import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.clases.TrackerClass;
import com.sistematias.rutear.model.Persistencia;
import com.sistematias.rutear.model.PuntoVenta;
import com.sistematias.rutear.model.Usuario;

import java.util.ArrayList;
import java.util.Vector;

public class MenuPrincipalActivity extends AppCompatActivity {
//    String[] options = new String[] {
//            "Nuevo Relevamiento",
//            "Novedad",
//            "Sincronización",
//            "Configuración"
//    };

    //    int[] iconos = new int[]{
//            R.drawable.icon_scan,
//            R.drawable.icon_anotation,
//            R.drawable.icon_sync,
//            R.drawable.icon_configuration
//    };
    public ArrayList<PuntoVenta> puntoVentaArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Persistencia persistencia = new Persistencia();
        persistencia.getPersistenciaByCod(Persistencia.COD_TIPO_USUARIO);
        switch (persistencia.getValuePersistencia().trim()) {
            case Usuario.COD_ADMIN:
                setContentView(R.layout.activity_menu_principal_admin);
                break;
            case Usuario.COD_REPOSITOR:
                setContentView(R.layout.activity_menu_principal_repositor);
                break;
            default:
                break;
        }

        getSupportActionBar().hide();
    }

    public void menu_item_1_onClickListener(View v) {
        //if (!isGPSEnabled(MenuPrincipalActivity.this)) {
        //showSettingsAlert();
        //}else{
        Intent activityChangeIntent = new Intent(MenuPrincipalActivity.this, ListaProducto.class);
        startActivity(activityChangeIntent);
        //}
    }

    public void menu_item_2_onClickListener(View v) {
        //if (!isGPSEnabled(MenuPrincipalActivity.this)) {
        //showSettingsAlert();
        //}else{
        Intent intentNovedadActivity = new Intent(MenuPrincipalActivity.this, NovedadActivity.class);
        startActivity(intentNovedadActivity);
        //}
    }

    public void menu_item_3_onClickListener(View v) {
        Intent intent = new Intent(MenuPrincipalActivity.this, SincronizacionActivity.class);
        startActivity(intent);
    }

    public void menu_item_4_onClickListener(View v) {
        Intent ConfiguracionActivityIntent = new Intent(MenuPrincipalActivity.this, ConfiguracionActivity.class);
        startActivity(ConfiguracionActivityIntent);
    }

    public void menu_item_5_onClickListener(View v) {
        Usuario usuario = Usuario.getUsuarioByCodUser(Data.getCodRepositor());
        Vector<PuntoVenta> puntosVentas = usuario.getPuntosVentas();
        puntoVentaArrayList.clear();
        for (int i = 0; i < puntosVentas.size(); i++) {
            puntoVentaArrayList.add(puntosVentas.get(i));
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        ArrayAdapterPuntosVentas adapter = new ArrayAdapterPuntosVentas(this, puntoVentaArrayList);
        alertDialogBuilder
                .setTitle("Seleccionar un punto de venta")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PuntoVenta pv = puntoVentaArrayList.get(which);
                        Data.setCodPuntoVentaActual(pv.getCodPuntoVenta());
                        Persistencia persistenciaCodPVA = new Persistencia();
                        persistenciaCodPVA.getPersistenciaByCod(Persistencia.COD_PUNTO_VENTA_ACTUAL);
                        persistenciaCodPVA.setValuePersistencia(pv.getCodPuntoVenta());
                        persistenciaCodPVA.setData();

                        Data.setNombrePuntoVentaActual(pv.getNombre());
                        Persistencia persistenciaPVA = new Persistencia();
                        persistenciaPVA.getPersistenciaByCod(Persistencia.COD_NOMBRE_PUNTO_VENTA_ACTUAL);
                        persistenciaPVA.setValuePersistencia(pv.getNombre());
                        persistenciaPVA.setData();

                        Data.setCodSucursalActual(pv.getCodSucursal());
                        Persistencia persistenciaSucursal = new Persistencia();
                        persistenciaSucursal.getPersistenciaByCod(Persistencia.COD_SUCURSAL);
                        persistenciaSucursal.setValuePersistencia(pv.getCodSucursal());
                        persistenciaSucursal.setData();

                        ((TextView) findViewById(R.id.txt_titlebar)).setText(Data.getNombrePuntoVentaActual().trim());
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isGPSEnabled(Context mContext) {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Configuración GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS no está habilitado.\n Para poder utilizar esta aplicacción es necesario habilitar GPS.\n¿Desea habilitar GPS?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Configurar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                MenuPrincipalActivity.this.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.txt_titlebar)).setText(Data.getNombrePuntoVentaActual().trim());
        ((TextView) findViewById(R.id.txtLastSession)).setText("Ultimo inicio de sesión: " + Data.getFechaUltimoInicioSesion());
    }

    public void exitOnClickListener(View v) {
        showExitMessage();
    }

    public void showExitMessage() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Salir");

        // Setting Dialog Message
        alertDialog.setMessage("¿Desea cerrar la sesión?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    ConnectionChecker.getInstance().setIsWorking(false);
                    ConnectionChecker.getInstance().getAsyncTask().cancel(true);
                    ConnectionChecker.getInstance().setAsyncTask(null);
                } catch (Exception e2) {

                }

                /*detener servicio de tracking*/
                TrackerClass.stopTracking();
                //if (TrackerService.isRunning()) {
                //    stopService(new Intent(MenuPrincipalActivity.this, TrackerService.class));
                //}
                /*if (mBound && Data.isServiceActive()) {
                    gpsTracker.stopUsingGPS();
                    Data.setServiceActive(false);
                }*/
                Persistencia.limpiarPersistencia();
                finish();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showExitMessage();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        //Log.i("service", "service start");
        //Intent intent = new Intent(this, GPSTracker.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    //GPSTracker gpsTracker;
    //boolean mBound = false;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    /*private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Log.i("local binder", "at to local binder");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            GPSTracker.LocalBinder binder = (GPSTracker.LocalBinder) service;
            gpsTracker = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };*/
}