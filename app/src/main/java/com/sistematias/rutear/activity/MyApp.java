package com.sistematias.rutear.activity;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

/**
 * Created by samuel on 19/09/2015.
 */
public class MyApp extends MultiDexApplication {
    private static Context context;
    public void onCreate(){
        super.onCreate();
        MyApp.context = getApplicationContext();
    }

    public static Context getAppContext(){
        return context;
    }
}
