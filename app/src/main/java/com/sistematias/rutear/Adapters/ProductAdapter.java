package com.sistematias.rutear.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sistematias.rutear.R;
import com.sistematias.rutear.model.Producto;

import java.util.List;

/**
 * Created by RUBEN on 03/03/2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.CustomViewHolder> {
    Context context;
    private List<Producto> lista_Productos;
    private Object tag;
    private boolean animate = false;

    public List<Producto> getLista_Productos() {
        return lista_Productos;
    }

    public void setLista_Productos(List<Producto> lista_Productos) {
        this.lista_Productos = lista_Productos;
        this.notifyDataSetChanged();
    }

    public ProductAdapter(Context context, List<Producto> lista_Productos) {
        this.context = context;
        this.lista_Productos = lista_Productos;
    }

    @Override
    public ProductAdapter.CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lista_producto_item, viewGroup, false);
        ProductAdapter.CustomViewHolder viewHolder = new ProductAdapter.CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.CustomViewHolder holder, int position) {
        // set the data in items
        try {
            holder.productName.setText(lista_Productos.get(position).getNombre());
            holder.productEan.setText(lista_Productos.get(position).getEan());
            holder.productPresentacion.setText(lista_Productos.get(position).getPresentacion());
            holder.productBrand.setText(lista_Productos.get(position).getMarca());
            if (animate) {
                Animation animation = AnimationUtils.loadAnimation(context, R.anim.enter_from_left_list);
                animation.reset();
                holder.itemView.startAnimation(animation);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return (null != lista_Productos ? lista_Productos.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView productName;
        TextView productEan;
        TextView productBrand;
        TextView productPresentacion;

        public CustomViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            productName = itemView.findViewById(R.id.product_name);
            productEan = itemView.findViewById(R.id.product_ean);
            productBrand = itemView.findViewById(R.id.product_brand);
            productPresentacion = itemView.findViewById(R.id.product_presentacion);
        }

        public void clearAnimation() {
            itemView.clearAnimation();
        }

    }

    @Override
    public void onViewDetachedFromWindow(final CustomViewHolder holder) {
        ((CustomViewHolder) holder).clearAnimation();
    }

    public void setAnimate(boolean animate) {
        this.animate = animate;
    }

    public void clear() {
        int size = this.lista_Productos.size();
        this.lista_Productos.clear();
        notifyItemRangeRemoved(0, size);
    }
}
