package com.sistematias.rutear.Services;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.model.Tracking;

import java.util.Date;

public class GPSTracker extends Service implements LocationListener {

    private Context mContext;
    private static final String GPS_MANAGER_BROADCAST_RECEIVER = "GPS_MANAGER_BROADCAST_RECEIVER";
    private final IBinder mBinder = new LocalBinder();


    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    double accuracy;// acuracy

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    // The minimum time between updates in milliseconds
    private static long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    /**/

    public GPSTracker() {
        mContext = null;
    }

    public GPSTracker(Context context) {
        Log.i("location", "listener lcoation");
        this.mContext = context;
        //location = getLocation(context);
    }

    public Location getLocation(Context context, int time) {
        MIN_TIME_BW_UPDATES = 1000*60*time;
        Log.e("inicia trackink con: ",time + " minutos");
        //MIN_TIME_BW_UPDATES = 10000;
        this.mContext = context;
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                Log.e("no hay gps internet?", "no ay gps internet?");
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            0, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                } else {
                    Log.e("no hay gps", "no ay gps");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get Accuracy
     */
    public double getAccuracy() {
        if (location != null) {
            accuracy = location.getAccuracy();
        }

        // return latitude
        return accuracy;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("Configuración GPS");

        // Setting Dialog Message
        alertDialog.setMessage("GPS no esta habilitado.\n para poder utilizar esta aplicacción es necesario habilitar GPS.\n¿Desea habilitar GPS?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("cambio gps", "cambio location");
        Log.i("hora", Data.FECHA_DATABASE_FORMAT.format(new Date()));
        Log.i("latitud", location.getLatitude() + "");
        Log.i("longitud", location.getLongitude() + "");
        Log.i("precision", location.getAccuracy() + "");


        if(!Data.getCodPuntoVentaActual().isEmpty() && !Data.getCodSucursalActual().isEmpty()){
            Tracking tracking = new Tracking();
            tracking.setLatitud(location.getLatitude() + "");
            tracking.setLongitud(location.getLongitude() + "");
            tracking.setPrecision((int) location.getAccuracy());
            tracking.setFechaCaptura(Data.FECHA_DATABASE_FORMAT.format(new Date()));
            tracking.setCodRepositor(Data.getCodRepositor());
            tracking.setCodPuntoVenta(Data.getCodPuntoVentaActual());
            tracking.setCodSucursal(Data.getCodSucursalActual());
            tracking.saveData();
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("==================", "==============================");
        Log.i("onProviderDisabled", "==============================");
        Log.i("==================", "==============================");

        //this.stopUsingGPS();

        //Persistencia persistencia = new Persistencia();
        //persistencia.getPersistenciaByCod(Persistencia.COD_TIPO_USUARIO);
        //if(persistencia.getValuePersistencia().equals(Usuario.COD_REPOSITOR)){
            //Intent in=new Intent().setClass(GPSTracker.this,LoginActivity.class);
            //in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //in.putExtra("disabled","disabled");
            //startActivity(in);
//        Intent intent = new Intent(GPS_MANAGER_BROADCAST_RECEIVER);
//        intent.putExtra("statusGPS", "DISABLED");
//        MyApp.getAppContext().sendBroadcast(intent);
        //}
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i("=================", "==============================");
        Log.i("onProviderEnabled", "==============================");
        Log.i("=================", "==============================");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public GPSTracker getService() {
            // Return this instance of LocalService so clients can call public methods
            return GPSTracker.this;
        }
    }


}
