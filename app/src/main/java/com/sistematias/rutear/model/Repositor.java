package com.sistematias.rutear.model;

import android.app.Application;
import android.database.Cursor;

import com.sistematias.rutear.clases.Data;
import com.sistematias.rutear.clases.Querys;

import java.util.Vector;

/**
 * Created by samuel on 19/09/2015.
 */
public class Repositor extends Application {
    private int idRepositor = 0;
    private String codRepositor = "";
    private String nombres = "";
    private String apellido = "";
    private String pass = "";
    private boolean habilitado = true;
    private String legajo = "";
    private String imagen = "";
    private Vector<PuntoVenta> puntoVentaDelRepositor = new Vector<>();

    public Vector<PuntoVenta> getPuntoVentaDelRepositor() {
        return puntoVentaDelRepositor;
    }
    public String getLegajo() { return legajo; }

    public void setLegajo(String legajo) { this.legajo = legajo; }

    public String getImagen() {return imagen; }

    public void setImagen(String imagen) { this.imagen = imagen; }


    public int getIdRepositor() {
        return idRepositor;
    }

    public void setIdRepositor(int idRepositor) {
        this.idRepositor = idRepositor;
    }

    public String getCodRepositor() {
        return codRepositor;
    }

    public void setCodRepositor(String codRepositor) {
        this.codRepositor = codRepositor;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public static Repositor checkUsuario(String legajo, String pass) {
        Repositor repositor=null;
        Cursor cursor = null;
        cursor = Data.getConn().executeSelect(Querys.validarRepositor(legajo,pass),cursor);
        while (cursor.moveToNext()) {
            repositor = new Repositor();
            repositor.idRepositor = cursor.getInt(0);
            repositor.codRepositor = cursor.getString(1);
            repositor.legajo = cursor.getString(2);
            repositor.apellido = cursor.getString(3);
            repositor.nombres = cursor.getString(4);
            repositor.imagen = cursor.getString(5);
            repositor.pass = cursor.getString(6);
            repositor.habilitado = cursor.getInt(7)==1;
            /*cargar sucursales*/
            Cursor cursorPuntosVenta = null;
            cursorPuntosVenta = Data.getConn().executeSelect(Querys.getSucursalesByRepositor(repositor.legajo),cursorPuntosVenta);
            while (cursorPuntosVenta.moveToNext()){
                PuntoVenta puntoVenta = new PuntoVenta();
                puntoVenta.setIdPuntoVenta(cursorPuntosVenta.getInt(0));
                puntoVenta.setCodPuntoVenta(cursorPuntosVenta.getString(1));
                puntoVenta.setNombre(cursorPuntosVenta.getString(2));
                puntoVenta.setCodSucursal(cursorPuntosVenta.getString(3));
                puntoVenta.setDireccion(cursorPuntosVenta.getString(4));
                puntoVenta.setCiudad(cursorPuntosVenta.getString(5));
                puntoVenta.setProvincia(cursorPuntosVenta.getString(6));
                puntoVenta.setZona(cursorPuntosVenta.getString(7));
                puntoVenta.setCoordenadas(cursorPuntosVenta.getString(8));
                puntoVenta.setTipoCliente(cursorPuntosVenta.getString(9));
                puntoVenta.setImagen(cursorPuntosVenta.getString(10));
                puntoVenta.setHabilitado(cursorPuntosVenta.getInt(11)==1);
                repositor.puntoVentaDelRepositor.add(puntoVenta);
            }
            cursorPuntosVenta.close();
        }
        cursor.close();
        return repositor;
    }
}
